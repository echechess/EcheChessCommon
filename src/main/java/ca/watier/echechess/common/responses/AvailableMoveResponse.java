/*
 *    Copyright 2014 - 2018 Yannick Watier
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ca.watier.echechess.common.responses;

import java.util.*;

public class AvailableMoveResponse {
    private String from;
    private final Set<String> positions = new HashSet<>();

    public AvailableMoveResponse() {
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public AvailableMoveResponse withFrom(String from) {
        this.from = from;
        return this;
    }

    public Set<String> getPositions() {
        return Collections.unmodifiableSet(positions);
    }

    public void setPositions(List<String> positions) {
        this.positions.clear();
        this.positions.addAll(positions);
    }

    public void addPosition(String position) {
        positions.add(position);
    }

    public AvailableMoveResponse withPositions(List<String> positions) {
        setPositions(positions);
        return this;
    }

    public AvailableMoveResponse withPositions(String... positions) {
        if (positions != null) {
            setPositions(Arrays.asList(positions));
        }

        return this;
    }
}
