/*
 *    Copyright 2014 - 2018 Yannick Watier
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ca.watier.echechess.common.enums;

/**
 * Created by yannick on 5/1/2017.
 */
public enum ChessEventMessage {
    AVAILABLE_MOVE, GAME_WON, GAME_WON_EVENT_MOVE, KING_CHECK, KING_CHECKMATE, MOVE, PAWN_PROMOTION,
    PLAYER_JOINED, PLAYER_KING_CHECK, PLAYER_TURN, REFRESH_BOARD,
    SCORE_UPDATE, TRY_JOIN_GAME, UI_SESSION_ALREADY_INITIALIZED, UI_SESSION_EXPIRED
}
