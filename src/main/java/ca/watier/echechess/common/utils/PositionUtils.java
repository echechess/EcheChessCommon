/*
 *    Copyright 2014 - 2018 Yannick Watier
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ca.watier.echechess.common.utils;

import ca.watier.echechess.common.enums.CasePosition;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

import static ca.watier.echechess.common.enums.CasePosition.*;

public class PositionUtils {
    private static final EnumSet<CasePosition> COL_A = EnumSet.of(A1, A2, A3, A4, A5, A6, A7, A8);
    private static final EnumSet<CasePosition> COL_B = EnumSet.of(B1, B2, B3, B4, B5, B6, B7, B8);
    private static final EnumSet<CasePosition> COL_C = EnumSet.of(C1, C2, C3, C4, C5, C6, C7, C8);
    private static final EnumSet<CasePosition> COL_D = EnumSet.of(D1, D2, D3, D4, D5, D6, D7, D8);
    private static final EnumSet<CasePosition> COL_E = EnumSet.of(E1, E2, E3, E4, E5, E6, E7, E8);
    private static final EnumSet<CasePosition> COL_F = EnumSet.of(F1, F2, F3, F4, F5, F6, F7, F8);
    private static final EnumSet<CasePosition> COL_G = EnumSet.of(G1, G2, G3, G4, G5, G6, G7, G8);
    private static final EnumSet<CasePosition> COL_H = EnumSet.of(H1, H2, H3, H4, H5, H6, H7, H8);
    private static final EnumSet<CasePosition> ROW_EIGHT = EnumSet.of(A8, B8, C8, D8, E8, F8, G8, H8);
    private static final EnumSet<CasePosition> ROW_SEVEN = EnumSet.of(A7, B7, C7, D7, E7, F7, G7, H7);
    private static final EnumSet<CasePosition> ROW_SIX = EnumSet.of(A6, B6, C6, D6, E6, F6, G6, H6);
    private static final EnumSet<CasePosition> ROW_FIVE = EnumSet.of(A5, B5, C5, D5, E5, F5, G5, H5);
    private static final EnumSet<CasePosition> ROW_FOUR = EnumSet.of(A4, B4, C4, D4, E4, F4, G4, H4);
    private static final EnumSet<CasePosition> ROW_THREE = EnumSet.of(A3, B3, C3, D3, E3, F3, G3, H3);
    private static final EnumSet<CasePosition> ROW_TWO = EnumSet.of(A2, B2, C2, D2, E2, F2, G2, H2);
    private static final EnumSet<CasePosition> ROW_ONE = EnumSet.of(A1, B1, C1, D1, E1, F1, G1, H1);

    private static final List<EnumSet<CasePosition>> BOARD = List.of(
            ROW_EIGHT,
            ROW_SEVEN,
            ROW_SIX,
            ROW_FIVE,
            ROW_FOUR,
            ROW_THREE,
            ROW_TWO,
            ROW_ONE
    );

    public static List<EnumSet<CasePosition>> getBoard() {
        return BOARD;
    }

    public static CasePosition getCasePositionByCoor(int x, int y) {
        CasePosition position = null;

        EnumSet<CasePosition> toCheck;
        switch (x) {
            case -4:
                toCheck = COL_A;
                break;
            case -3:
                toCheck = COL_B;
                break;
            case -2:
                toCheck = COL_C;
                break;
            case -1:
                toCheck = COL_D;
                break;
            case 0:
                toCheck = COL_E;
                break;
            case 1:
                toCheck = COL_F;
                break;
            case 2:
                toCheck = COL_G;
                break;
            case 3:
                toCheck = COL_H;
                break;
            default:
                return null;
        }

        for (CasePosition casePosition : toCheck) {
            if (y == casePosition.getY()) {
                position = casePosition;
                break;
            }
        }

        return position;
    }

    public static List<CasePosition> getAllPositionFromColumn(char value) {
        value = Character.toLowerCase(value);
        List<CasePosition> values = new ArrayList<>();

        if (value >= 'a' && value <= 'h') {
            for (CasePosition casePosition : values()) {
                if (casePosition.getCol() == value) {
                    values.add(casePosition);
                }
            }
        }

        return values;
    }

    public static List<CasePosition> getAllPositionsFromRow(int value) {
        value = Character.toLowerCase(value);
        List<CasePosition> values = new ArrayList<>();

        if (value >= 1 && value <= 8) {
            for (CasePosition casePosition : values()) {
                if (casePosition.getRow() == value) {
                    values.add(casePosition);
                }
            }
        }

        return values;
    }
}
