/*
 *    Copyright 2014 - 2018 Yannick Watier
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ca.watier.echechess.common.sessions;

import java.io.Serial;
import java.io.Serializable;
import java.util.*;

/**
 * Created by yannick on 4/17/2017.
 */
public class Player implements Serializable {

    @Serial
    private static final long serialVersionUID = 296605081563013686L;

    private final String uuid;
    private final List<UUID> createdGameList = new ArrayList<>();
    private final List<UUID> joinedGameList = new ArrayList<>();
    private final List<UUID> uiSessionList = new ArrayList<>();

    public Player(String uuid) {
        this.uuid = uuid;
    }

    public void addCreatedGame(UUID uuid) {
        if (uuid == null) {
            return;
        }

        createdGameList.add(uuid);
    }

    public void addJoinedGame(UUID uuid) {
        if (uuid == null) {
            return;
        }

        joinedGameList.add(uuid);
    }

    public void addUiSession(UUID uuid) {
        if (uuid == null) {
            return;
        }

        uiSessionList.add(uuid);
    }

    public List<UUID> getCreatedGameList() {
        return Collections.unmodifiableList(createdGameList);
    }

    public List<UUID> getJoinedGameList() {
        return Collections.unmodifiableList(joinedGameList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(uuid, createdGameList, joinedGameList, uiSessionList);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Player player = (Player) o;
        return Objects.equals(uuid, player.uuid) &&
                Objects.equals(createdGameList, player.createdGameList) &&
                Objects.equals(joinedGameList, player.joinedGameList) &&
                Objects.equals(uiSessionList, player.uiSessionList);
    }
}
