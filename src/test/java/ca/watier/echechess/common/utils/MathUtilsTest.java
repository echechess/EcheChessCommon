/*
 *    Copyright 2014 - 2018 Yannick Watier
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package ca.watier.echechess.common.utils;


import ca.watier.echechess.common.enums.CasePosition;
import ca.watier.echechess.common.enums.Direction;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;


/**
 * Created by yannick on 4/25/2017.
 */
public class MathUtilsTest {

    public static final float KNIGHT_RADIUS_EQUATION = 2.23606797749979f;
    private static final Direction NORTH = Direction.NORTH;
    private static final Direction NORTH_WEST = Direction.NORTH_WEST;
    private static final Direction WEST = Direction.WEST;
    private static final Direction SOUTH_WEST = Direction.SOUTH_WEST;
    private static final Direction SOUTH = Direction.SOUTH;
    private static final Direction SOUTH_EAST = Direction.SOUTH_EAST;
    private static final Direction EAST = Direction.EAST;
    private static final Direction NORTH_EAST = Direction.NORTH_EAST;
    private static final CasePosition D_5 = CasePosition.D5;
    private static final float DELTA_SLOPE_TEST = 0f;

    @Test
    public void isPositionVertical() {
        assertTrue(MathUtils.isPositionVertical(CasePosition.A8, CasePosition.A1));
        assertTrue(MathUtils.isPositionVertical(CasePosition.A1, CasePosition.A8));
        assertTrue(MathUtils.isPositionVertical(CasePosition.A4, CasePosition.A6));
        assertFalse(MathUtils.isPositionVertical(CasePosition.A4, CasePosition.B4));
    }

    @Test
    public void isPositionHorizontal() {
        assertTrue(MathUtils.isPositionHorizontal(CasePosition.B1, CasePosition.A1));
        assertTrue(MathUtils.isPositionHorizontal(CasePosition.A8, CasePosition.B8));
        assertTrue(MathUtils.isPositionHorizontal(CasePosition.A6, CasePosition.B6));
        assertFalse(MathUtils.isPositionHorizontal(CasePosition.A4, CasePosition.A2));
    }

    @Test
    public void getPositionsBetweenTwoPosition() {
        assertThat(MathUtils.getPositionsBetweenTwoPosition(CasePosition.A1, CasePosition.H8)).containsOnly(CasePosition.B2, CasePosition.C3, CasePosition.D4, CasePosition.E5, CasePosition.F6, CasePosition.G7);
        assertThat(MathUtils.getPositionsBetweenTwoPosition(CasePosition.H8, CasePosition.A1)).containsOnly(CasePosition.B2, CasePosition.C3, CasePosition.D4, CasePosition.E5, CasePosition.F6, CasePosition.G7);
        assertThat(MathUtils.getPositionsBetweenTwoPosition(CasePosition.E1, CasePosition.E8)).containsOnly(CasePosition.E2, CasePosition.E3, CasePosition.E4, CasePosition.E5, CasePosition.E6, CasePosition.E7);
        assertThat(MathUtils.getPositionsBetweenTwoPosition(CasePosition.E8, CasePosition.E1)).containsOnly(CasePosition.E2, CasePosition.E3, CasePosition.E4, CasePosition.E5, CasePosition.E6, CasePosition.E7);
        assertThat(MathUtils.getPositionsBetweenTwoPosition(CasePosition.A4, CasePosition.H4)).containsOnly(CasePosition.B4, CasePosition.C4, CasePosition.D4, CasePosition.E4, CasePosition.F4, CasePosition.G4);
        assertThat(MathUtils.getPositionsBetweenTwoPosition(CasePosition.H4, CasePosition.A4)).containsOnly(CasePosition.B4, CasePosition.C4, CasePosition.D4, CasePosition.E4, CasePosition.F4, CasePosition.G4);
        assertThat(MathUtils.getPositionsBetweenTwoPosition(CasePosition.A8, CasePosition.H1)).containsOnly(CasePosition.B7, CasePosition.C6, CasePosition.D5, CasePosition.E4, CasePosition.F3, CasePosition.G2);
        assertThat(MathUtils.getPositionsBetweenTwoPosition(CasePosition.H1, CasePosition.A8)).containsOnly(CasePosition.B7, CasePosition.C6, CasePosition.D5, CasePosition.E4, CasePosition.F3, CasePosition.G2);
        assertThat(MathUtils.getPositionsBetweenTwoPosition(CasePosition.E4, CasePosition.G4)).containsOnly(CasePosition.F4);
        assertThat(MathUtils.getPositionsBetweenTwoPosition(CasePosition.F3, CasePosition.F5)).containsOnly(CasePosition.F4);
        assertThat(MathUtils.getPositionsBetweenTwoPosition(CasePosition.E3, CasePosition.G5)).containsOnly(CasePosition.F4);
        assertThat(MathUtils.getPositionsBetweenTwoPosition(CasePosition.G5, CasePosition.E3)).containsOnly(CasePosition.F4);
        assertThat(MathUtils.getPositionsBetweenTwoPosition(CasePosition.A4, CasePosition.H8)).isEmpty();
    }

    @Test
    public void isPositionOnCirclePerimeter_knight() {

        int x = D_5.getX();
        int y = D_5.getY();

        assertTrue(MathUtils.isPositionOnCirclePerimeter(D_5, CasePosition.B4, x + KNIGHT_RADIUS_EQUATION, y));
        assertTrue(MathUtils.isPositionOnCirclePerimeter(D_5, CasePosition.B6, x + KNIGHT_RADIUS_EQUATION, y));

        assertTrue(MathUtils.isPositionOnCirclePerimeter(D_5, CasePosition.C7, x + KNIGHT_RADIUS_EQUATION, y));
        assertTrue(MathUtils.isPositionOnCirclePerimeter(D_5, CasePosition.E7, x + KNIGHT_RADIUS_EQUATION, y));

        assertTrue(MathUtils.isPositionOnCirclePerimeter(D_5, CasePosition.F4, x + KNIGHT_RADIUS_EQUATION, y));
        assertTrue(MathUtils.isPositionOnCirclePerimeter(D_5, CasePosition.F6, x + KNIGHT_RADIUS_EQUATION, y));

        assertTrue(MathUtils.isPositionOnCirclePerimeter(D_5, CasePosition.C3, x + KNIGHT_RADIUS_EQUATION, y));
        assertTrue(MathUtils.isPositionOnCirclePerimeter(D_5, CasePosition.E3, x + KNIGHT_RADIUS_EQUATION, y));

        assertFalse(MathUtils.isPositionOnCirclePerimeter(D_5, CasePosition.D7, x + KNIGHT_RADIUS_EQUATION, y));
        assertFalse(MathUtils.isPositionOnCirclePerimeter(D_5, CasePosition.D3, x + KNIGHT_RADIUS_EQUATION, y));
        assertFalse(MathUtils.isPositionOnCirclePerimeter(D_5, CasePosition.B5, x + KNIGHT_RADIUS_EQUATION, y));
        assertFalse(MathUtils.isPositionOnCirclePerimeter(D_5, CasePosition.F5, x + KNIGHT_RADIUS_EQUATION, y));
    }

    @Test
    public void getDistanceBetweenPositions() {
        assertThat(MathUtils.getDistanceBetweenPositionsWithCommonDirection(D_5, D_5)).isEqualTo(0);
        assertThat(MathUtils.getDistanceBetweenPositionsWithCommonDirection(CasePosition.H1, CasePosition.H8)).isEqualTo(7);
        assertThat(MathUtils.getDistanceBetweenPositionsWithCommonDirection(CasePosition.H1, CasePosition.H5)).isEqualTo(4);
        assertThat(MathUtils.getDistanceBetweenPositionsWithCommonDirection(CasePosition.H8, CasePosition.H1)).isEqualTo(7);
        assertThat(MathUtils.getDistanceBetweenPositionsWithCommonDirection(CasePosition.H5, CasePosition.H1)).isEqualTo(4);
        assertThat(MathUtils.getDistanceBetweenPositionsWithCommonDirection(CasePosition.H5, CasePosition.A5)).isEqualTo(7);
        assertThat(MathUtils.getDistanceBetweenPositionsWithCommonDirection(CasePosition.H5, CasePosition.E5)).isEqualTo(3);
        assertThat(MathUtils.getDistanceBetweenPositionsWithCommonDirection(CasePosition.A5, CasePosition.H5)).isEqualTo(7);
        assertThat(MathUtils.getDistanceBetweenPositionsWithCommonDirection(CasePosition.E5, CasePosition.H5)).isEqualTo(3);
        assertThat(MathUtils.getDistanceBetweenPositionsWithCommonDirection(CasePosition.H1, CasePosition.A8)).isEqualTo(9);
        assertThat(MathUtils.getDistanceBetweenPositionsWithCommonDirection(CasePosition.F3, CasePosition.A8)).isEqualTo(7);
        assertThat(MathUtils.getDistanceBetweenPositionsWithCommonDirection(CasePosition.A8, CasePosition.H1)).isEqualTo(9);
        assertThat(MathUtils.getDistanceBetweenPositionsWithCommonDirection(CasePosition.A8, CasePosition.F3)).isEqualTo(7);
    }

    @Test
    public void getNearestPositionFromDirection() {
        assertEquals(CasePosition.D6, MathUtils.getNearestPositionFromDirection(D_5, Direction.NORTH));
        assertEquals(CasePosition.D4, MathUtils.getNearestPositionFromDirection(D_5, Direction.SOUTH));
        assertEquals(CasePosition.C5, MathUtils.getNearestPositionFromDirection(D_5, Direction.WEST));
        assertEquals(CasePosition.E5, MathUtils.getNearestPositionFromDirection(D_5, Direction.EAST));

        assertEquals(CasePosition.C6, MathUtils.getNearestPositionFromDirection(D_5, Direction.NORTH_WEST));
        assertEquals(CasePosition.E6, MathUtils.getNearestPositionFromDirection(D_5, Direction.NORTH_EAST));

        assertEquals(CasePosition.C4, MathUtils.getNearestPositionFromDirection(D_5, Direction.SOUTH_WEST));
        assertEquals(CasePosition.E4, MathUtils.getNearestPositionFromDirection(D_5, Direction.SOUTH_EAST));


        assertEquals(CasePosition.D7, MathUtils.getNearestPositionFromDirection(D_5, Direction.NORTH, 2));
        assertEquals(CasePosition.D3, MathUtils.getNearestPositionFromDirection(D_5, Direction.SOUTH, 2));
        assertEquals(CasePosition.B5, MathUtils.getNearestPositionFromDirection(D_5, Direction.WEST, 2));
        assertEquals(CasePosition.F5, MathUtils.getNearestPositionFromDirection(D_5, Direction.EAST, 2));

        assertEquals(CasePosition.B7, MathUtils.getNearestPositionFromDirection(D_5, Direction.NORTH_WEST, 2));
        assertEquals(CasePosition.F7, MathUtils.getNearestPositionFromDirection(D_5, Direction.NORTH_EAST, 2));

        assertEquals(CasePosition.B3, MathUtils.getNearestPositionFromDirection(D_5, Direction.SOUTH_WEST, 2));
        assertEquals(CasePosition.F3, MathUtils.getNearestPositionFromDirection(D_5, Direction.SOUTH_EAST, 2));
    }

    @Test
    public void getDirectionFromPosition() {
        assertNull(MathUtils.getDirectionFromPosition(D_5, D_5));

        assertEquals(NORTH, MathUtils.getDirectionFromPosition(D_5, CasePosition.D6));
        assertEquals(NORTH, MathUtils.getDirectionFromPosition(D_5, CasePosition.D8));

        assertEquals(NORTH_WEST, MathUtils.getDirectionFromPosition(D_5, CasePosition.C8));
        assertEquals(NORTH_WEST, MathUtils.getDirectionFromPosition(D_5, CasePosition.C6));
        assertEquals(NORTH_WEST, MathUtils.getDirectionFromPosition(D_5, CasePosition.A8));

        assertEquals(WEST, MathUtils.getDirectionFromPosition(D_5, CasePosition.A5));
        assertEquals(WEST, MathUtils.getDirectionFromPosition(D_5, CasePosition.C5));

        assertEquals(SOUTH_WEST, MathUtils.getDirectionFromPosition(D_5, CasePosition.B4));
        assertEquals(SOUTH_WEST, MathUtils.getDirectionFromPosition(D_5, CasePosition.C4));
        assertEquals(SOUTH_WEST, MathUtils.getDirectionFromPosition(D_5, CasePosition.A1));

        assertEquals(SOUTH, MathUtils.getDirectionFromPosition(D_5, CasePosition.D4));
        assertEquals(SOUTH, MathUtils.getDirectionFromPosition(D_5, CasePosition.D1));

        assertEquals(SOUTH_EAST, MathUtils.getDirectionFromPosition(D_5, CasePosition.F1));
        assertEquals(SOUTH_EAST, MathUtils.getDirectionFromPosition(D_5, CasePosition.E4));
        assertEquals(SOUTH_EAST, MathUtils.getDirectionFromPosition(D_5, CasePosition.H1));

        assertEquals(EAST, MathUtils.getDirectionFromPosition(D_5, CasePosition.E5));
        assertEquals(EAST, MathUtils.getDirectionFromPosition(D_5, CasePosition.H5));

        assertEquals(NORTH_EAST, MathUtils.getDirectionFromPosition(D_5, CasePosition.G6));
        assertEquals(NORTH_EAST, MathUtils.getDirectionFromPosition(D_5, CasePosition.E6));
        assertEquals(NORTH_EAST, MathUtils.getDirectionFromPosition(D_5, CasePosition.H8));
    }

    @Test
    public void getSlopeFromPosition() {
        assertNull(MathUtils.getSlopeFromPosition(D_5, D_5));
        assertEquals(-1f, MathUtils.getSlopeFromPosition(CasePosition.B8, CasePosition.H2), DELTA_SLOPE_TEST);
        assertEquals(7 / 3f, MathUtils.getSlopeFromPosition(CasePosition.E1, CasePosition.H8), DELTA_SLOPE_TEST);
    }

    @Test
    public void isPositionInLine() {
        assertTrue(MathUtils.isPositionInLine(CasePosition.D6, CasePosition.E5, CasePosition.H2));
        assertTrue(MathUtils.isPositionInLine(CasePosition.A6, CasePosition.B5, CasePosition.D3));
        assertTrue(MathUtils.isPositionInLine(CasePosition.H8, CasePosition.G7, CasePosition.C3));
        assertTrue(MathUtils.isPositionInLine(CasePosition.E4, CasePosition.D4, CasePosition.A4));
        assertTrue(MathUtils.isPositionInLine(CasePosition.E4, CasePosition.E5, CasePosition.E8));
        assertFalse(MathUtils.isPositionInLine(CasePosition.H8, CasePosition.G7, CasePosition.C4));
    }
}